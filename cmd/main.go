package main

import (
	"fmt"
	"gitlab.com/pet-vadim/checker/internal/repositories"
	notifierrepo "gitlab.com/pet-vadim/checker/internal/repositories/notifier"
	"gitlab.com/pet-vadim/checker/internal/services/notifier"
	"gitlab.com/pet-vadim/checker/internal/services/tester"
	notifierbroker "gitlab.com/pet-vadim/checker/internal/transport/notifier"
	testerbroker "gitlab.com/pet-vadim/checker/internal/transport/tester"
	"gitlab.com/pet-vadim/libs/logger"
	"os"
	"os/signal"
	"strconv"
	"syscall"
)

func main() {
	logger.Info("starting checker app")

	db := repositories.New(getDBConfig())
	repository := notifierrepo.New(db)
	publisher := notifierbroker.NewPublisher(getNotifierBrokerConf())
	notifierSrv := notifier.New(repository, publisher)
	checkerSrv := tester.New(notifierSrv)
	consumer := testerbroker.NewConsumer(checkerSrv, getTesterBrokerConf())

	go func() {
		consumer.StartConsume()
	}()

	// Listen for the interrupt signal.
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	logger.Info("received os.signal: " + (<-quit).String())

	publisher.Close()
	consumer.Close()
	if err := db.Close(); err != nil {
		logger.Error("fail db connection close " + err.Error())
	}

	logger.Info("exit app")
}

func getNotifierBrokerConf() *notifierbroker.RabbitMq {
	return &notifierbroker.RabbitMq{
		DataSource: os.Getenv("NOTIFIER_RMQ_DATA_SOURCE"),
		QueueName:  os.Getenv("NOTIFIER_RMQ_QUEUE_NAME"),
		Mandatory:  toBool(os.Getenv("NOTIFIER_RMQ_MANDATORY")),
		Durable:    toBool(os.Getenv("NOTIFIER_RMQ_DURABLE")),
		AutoDelete: toBool(os.Getenv("NOTIFIER_RMQ_AUTO_DELETE")),
		AutoAck:    toBool(os.Getenv("NOTIFIER_RMQ_AUTO_ACK")),
		Exclusive:  toBool(os.Getenv("NOTIFIER_RMQ_EXCLUSIVE")),
		NoLocal:    toBool(os.Getenv("NOTIFIER_RMQ_NO_LOCAL")),
		NoWait:     toBool(os.Getenv("NOTIFIER_RMQ_NO_WAIT")),
		Args:       nil,
	}
}

func getDBConfig() *repositories.Config {
	return &repositories.Config{
		DBUsername: os.Getenv("DB_USERNAME"),
		DBPassword: os.Getenv("DB_PASSWORD"),
		DBHost:     os.Getenv("DB_HOST"),
		DBPort:     os.Getenv("DB_PORT"),
		DBTable:    os.Getenv("DB_TABLE"),
	}
}

func getTesterBrokerConf() *testerbroker.RabbitMq {
	return &testerbroker.RabbitMq{
		DataSource: os.Getenv("TESTER_RMQ_DATA_SOURCE"),
		QueueName:  os.Getenv("TESTER_RMQ_QUEUE_NAME"),
		Durable:    toBool(os.Getenv("TESTER_RMQ_DURABLE")),
		AutoDelete: toBool(os.Getenv("TESTER_RMQ_AUTO_DELETE")),
		AutoAck:    toBool(os.Getenv("TESTER_RMQ_AUTO_ACK")),
		Exclusive:  toBool(os.Getenv("TESTER_RMQ_EXCLUSIVE")),
		NoLocal:    toBool(os.Getenv("TESTER_RMQ_NO_LOCAL")),
		NoWait:     toBool(os.Getenv("TESTER_RMQ_NO_WAIT")),
		Args:       nil,
	}
}

func toBool(v string) bool {
	resBool, err := strconv.ParseBool(v)
	if err != nil {
		fmt.Println(v)
		logger.Fatal("fail convert env to bool " + err.Error())
	}
	return resBool
}
