package notifierrepo

import (
	"context"
	"database/sql"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/pet-vadim/checker/internal/services/notifier"
	"gitlab.com/pet-vadim/libs/errs"
	"gitlab.com/pet-vadim/libs/logger"
)

type repository struct {
	db *sqlx.DB
}

func New(db *sqlx.DB) notifier.Repository {
	return &repository{db: db}
}

func (r *repository) GetUserByID(ctx context.Context, userID string) (*notifier.User, *errs.AppError) {
	var user notifier.User
	query := `
	SELECT id, email
	FROM users
	WHERE id=$1;
	`
	err := r.db.GetContext(ctx, &user, query, userID)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Info("user was not found in db")
			return nil, errs.NewNotFoundError("user was not found")
		}
		logger.Error("err during getting user from db: " + err.Error())
		return nil, errs.NewUnexpectedError("database unexpected error")
	}

	return &user, nil
}
