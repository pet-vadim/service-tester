package repositories

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/pet-vadim/libs/logger"
)

type Config struct {
	DBUsername string
	DBPassword string
	DBHost     string
	DBPort     string
	DBTable    string
}

func New(c *Config) *sqlx.DB {
	var dbURL = fmt.Sprintf(
		"postgres://%v:%v@%v:%v/%v?sslmode=disable",
		c.DBUsername,
		c.DBPassword,
		c.DBHost,
		c.DBPort,
		c.DBTable,
	)
	db, err := sqlx.Connect("postgres", dbURL)
	if err != nil {
		logger.Fatal("Postgres connection failed " + err.Error())
	}

	return db
}
