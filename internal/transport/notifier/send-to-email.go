package notifierbroker

import (
	"encoding/json"
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/pet-vadim/libs/logger"
)

type Mail struct {
	ToEmail string `json:"to_email"`
	Subject string `json:"subject"`
	Msg     string `json:"msg"`
}

func (p *publisher) SendToMail(mail, msg string) {
	emailMsg := Mail{
		ToEmail: mail,
		Subject: "Monitoring fail",
		Msg:     msg,
	}

	bytes, err := json.Marshal(emailMsg)
	if err != nil {
		logger.Error("fail marshaling msg for email " + err.Error())
	}

	queueMsg := amqp.Publishing{
		DeliveryMode: amqp.Persistent,
		ContentType:  "application/json",
		Body:         bytes,
	}
	err = p.amqpDialCh.Publish(p.conf.Exchange, p.conf.QueueName, p.conf.Mandatory, p.conf.NoWait, queueMsg)
	if err != nil {
		logger.Error("failed to publish msg" + err.Error())
	}
}
