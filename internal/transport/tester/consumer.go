package testerbroker

import (
	"context"
	"encoding/json"
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/pet-vadim/checker/internal/services/tester"
	"gitlab.com/pet-vadim/libs/logger"
)

type consumer struct {
	tester     tester.Service
	amqpDial   *amqp.Connection
	amqpDialCh *amqp.Channel
	stop       chan bool
	conf       *RabbitMq
}

func NewConsumer(t tester.Service, c *RabbitMq) *consumer {
	amqpDial, err := amqp.Dial(c.DataSource)
	failOnError(err, "failed to connect to rabbitmq")

	ch, err := amqpDial.Channel()
	failOnError(err, "Failed to open a channel")

	_, err = ch.QueueDeclare(
		c.QueueName,
		c.Durable,
		c.AutoDelete,
		c.Exclusive,
		c.NoWait,
		c.Args,
	)
	failOnError(err, "failed to declare a queue")

	return &consumer{
		tester:     t,
		amqpDial:   amqpDial,
		amqpDialCh: ch,
		stop:       make(chan bool),
		conf:       c,
	}
}

func (c *consumer) StartConsume() {
	err := c.amqpDialCh.Qos(1, 0, false)
	failOnError(err, "failed to declare a queue")

	msgs, err := c.amqpDialCh.Consume(
		c.conf.QueueName,
		c.conf.Consumer,
		c.conf.AutoAck,
		c.conf.Exclusive,
		c.conf.NoLocal,
		c.conf.NoWait,
		nil,
	)
	failOnError(err, "failed to register a consumer")

	go func() {
		logger.Info("starting listening messages")
		for d := range msgs {
			go c.consumeMsg(&d)
		}
	}()

	<-c.stop
}

func (c *consumer) consumeMsg(d *amqp.Delivery) {
	var test tester.Test
	err := json.Unmarshal(d.Body, &test)
	if err != nil {
		logger.Error("failed to unmarshal rabbitmq msg: " + err.Error())
	}

	c.tester.Ping(context.Background(), &test)

	err = d.Ack(false)
	if err != nil {
		logger.Error("failed ack msg: " + err.Error())
	}
}

func (c *consumer) Close() {
	if err := c.amqpDialCh.Close(); err != nil {
		logger.Error("consumer channel close err: " + err.Error())
	}

	if err := c.amqpDial.Close(); err != nil {
		logger.Error("consumer conn close err: " + err.Error())
	}
}

func failOnError(err error, msg string) {
	if err != nil {
		logger.Fatal(msg + " " + err.Error())
	}
}

type RabbitMq struct {
	DataSource  string
	QueueName   string
	Consumer    string
	Exchange    string
	DeliveryKey string
	Durable     bool
	AutoDelete  bool
	AutoAck     bool
	Exclusive   bool
	NoLocal     bool
	NoWait      bool
	Args        map[string]interface{}
}

type Msg struct {
	TestType string      `json:"test_type"`
	UserID   string      `json:"user_id"`
	Test     tester.Test `json:"test"`
}
