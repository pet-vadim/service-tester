package notifier

import (
	"context"
)

type service struct {
	repo   Repository
	broker Broker
}

func New(repo Repository, broker Broker) Service {
	return &service{repo: repo, broker: broker}
}

func (s *service) Notify(ctx context.Context, userID, msg string) {
	user, appError := s.repo.GetUserByID(ctx, userID)
	if appError != nil {
		return
	}
	s.broker.SendToMail(user.Email, msg)
}
