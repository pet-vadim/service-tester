package notifier

import (
	"context"
	"gitlab.com/pet-vadim/libs/errs"
)

type Repository interface {
	GetUserByID(ctx context.Context, userID string) (*User, *errs.AppError)
}
