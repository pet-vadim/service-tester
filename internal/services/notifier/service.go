package notifier

import "context"

type Service interface {
	Notify(ctx context.Context, userID, msg string)
}
