package notifier

type Broker interface {
	SendToMail(mail string, msg string)
	Close()
}
