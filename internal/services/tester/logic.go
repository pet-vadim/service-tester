package tester

import (
	"context"
	"fmt"
	"gitlab.com/pet-vadim/checker/internal/services/notifier"
	"gitlab.com/pet-vadim/libs/closer"
	"gitlab.com/pet-vadim/libs/logger"
	"net/http"
	"strings"
)

type service struct {
	notifier notifier.Service
}

func New(n notifier.Service) *service {
	return &service{notifier: n}
}

func (s *service) Ping(ctx context.Context, test *Test) {
	// Act
	resp, err := http.Get(test.URL) // nolint
	if err != nil {
		logger.Error("error during get request: " + err.Error())
		return
	}
	defer closer.CheckAndLog(resp.Body)
	// Assert
	if resp.StatusCode != test.WontStatusCode {
		s.notifier.Notify(ctx, test.UserID, createMessage(test, resp.StatusCode))
	}
}

func createMessage(t *Test, respCode int) string {
	// Создание отдельно по строке, чтобы не путаться во множестве последовательных аргументов
	var sb strings.Builder
	sb.WriteString(fmt.Sprintf("%v \n", t.FailMsg))
	sb.WriteString("\n Test information:\n")
	sb.WriteString(fmt.Sprintf("Test Name: %v \n", t.Name))
	sb.WriteString(fmt.Sprintf("Expected status code is: %v \n", t.WontStatusCode))
	sb.WriteString(fmt.Sprintf("Received status code is: %v \n", respCode))
	return sb.String()
}
